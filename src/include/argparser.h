#ifndef ARG_PARSER_HEAD_HPP
#define ARG_PARSER_HEAD_HPP
#include "infocon.h"

int parse_head(int argc, char ** argv, struct ConnectionInfo * con);

#endif //ARG_PARSER_HEAD_HPP
