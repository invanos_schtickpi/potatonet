#ifndef SOCKET_CONNECTOR_HEAD_HPP
#define SOCKET_CONNECTOR_HEAD_HPP
#include "infocon.h"

#define CONNECTION_ERROR_SUCCESS {.err = 0, .status = NORMAL}

struct ConnectionError{
  enum {NORMAL, SOCKET, CONNECT, IP_LOOKUP} status;
  int err;
};

struct ConnectionError init_connection(struct ConnectionInfo * con);
void term_connection(struct ConnectionInfo * con);

#endif //SOCKET_CONNECTOR_HEAD_HPP
