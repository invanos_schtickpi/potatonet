#ifndef LOGGING_HEADER_HEAD_HPP
#define LOGGING_HEADER_HEAD_HPP
#include <stddef.h>
#include "infocon.h"

int open_log(struct ConnectionInfo *);
void close_log(struct ConnectionInfo *);

#endif //LOGGING_HEADER_HEAD_HPP
