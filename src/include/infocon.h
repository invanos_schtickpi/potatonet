#ifndef CONNECTION_INFO_DECLARATION_HEAD_HPP
#define CONNECTION_INFO_DECLARATION_HEAD_HPP

#define CONNECTION_INFO_INIT {.status=CLOSED, .socket=0, .domain=0, \
                              .port=0, .ip = {0},  \
                              .log = {.file=0, .status=SCLOSED, .name=0, }}
#define IP_LEN 46
struct ConnectionInfo
{
  char ip[IP_LEN];
  char const * domain;
  char const * port;
  struct{
    char const * name;
    int file;
    enum{SOPEN, SCLOSED} status;
  }log;
  int socket;
  enum{OPEN, CLOSED} status;
};


#endif //CONNECTION_INFO_DECLARATION_HEAD_HPP
