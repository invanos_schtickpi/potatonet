#include <unistd.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "logging.h"

int open_log(struct ConnectionInfo * con)
{
  assert(con);
  if(con->log.status == SOPEN){
    return -1;
  }
  if(con->log.name == NULL){
    con->log.file = 0;
    con->log.status = SCLOSED;
    return 0;
  }
  con->log.file = open(con->log.name, (O_WRONLY | O_APPEND | O_CREAT), 0644);
  if(con->log.file < 0){
    con->log.file = 0;
    return -1;
  }
  con->log.status = SOPEN;
  return 0;
}

void close_log(struct ConnectionInfo * con)
{
  if(con->log.status == SOPEN){
    close(con->log.file);
    con->log.status = SCLOSED;
    con->log.file = 0;
  }
}
