#include <argp.h>
#include "argparser.h"

#define DUMMY_STRING(x) #x
#define MACRO_TO_STRING(x) DUMMY_STRING(x)

static error_t parse_opt (int key, char *arg, struct argp_state *state);

char const * argp_program_version =
  "PotatoNet: " MACRO_TO_STRING(MAJOR_VER)
  "." MACRO_TO_STRING(MINOR_VER)
  "." MACRO_TO_STRING(PATCH_VER);

char const * argp_program_bug_address =
  "https://bitbucket.org/invanos_schtickpi/potatonet/issues";

int parse_head(int argc, char ** argv, struct ConnectionInfo * con)
{
  struct argp_option optVector[] = {

    {"file", 'f', "PATH", 0,
     "Path to logfile",0},
    {0, 0, 0, 0, 0, 0}
  };
  struct argp args = {
    optVector, parse_opt, "<Domain> <Port>",
    "PotatoNet: A simplistic copy of Telnet client. \v"
    "This program Will never be a feature complete product"
    "Will not do something more than a regular telnet, and probably will be less",
    NULL, NULL, NULL
  };
  if(argp_parse(&args, argc, argv, 0, NULL, con)){
    return -1;
  }
  return 0;
}

error_t parse_opt (int key, char *arg, struct argp_state *state)
{
  struct ConnectionInfo * con = (struct ConnectionInfo *)state->input;
  switch(key){
  case 'f':
    //    con->logFileName = arg;
    con->log.name = arg;
    return 0;
  case ARGP_KEY_ARG:
    if(state->arg_num == 0){
      con->domain = arg;
      return 0;
    }
    else if (state->arg_num == 1 ){
      con->port = arg;
      return 0;
      }
    else{
      argp_usage(state);
      return ARGP_ERR_UNKNOWN;
    }
  default:
    return ARGP_ERR_UNKNOWN;
  };
  return 0;
}
