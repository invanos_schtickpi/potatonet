#include <signal.h>
#include <stddef.h>
#include <errno.h>
#include <sys/wait.h>
#include "signaling.h"

static void sigdef_hand(int);
static void sigchd_hand(int);
static void sigalm_hand(int);

int set_signal(void)
{
  struct sigaction sh;
  sh.sa_handler = sigdef_hand;
  sigemptyset(&(sh.sa_mask));
  sh.sa_flags = 0;
  if (sigaction(SIGINT, &sh, NULL) == -1) {
    return -1;
  }
  if (sigaction(SIGTERM, &sh, NULL) == -1) {
    return -1;
  }
  if (sigaction(SIGHUP, &sh, NULL) == -1) {
    return -1;
  }
  sh.sa_handler = sigalm_hand;
  if (sigaction(SIGALRM, &sh, NULL) == -1) {
    return -1;
  }
  sh.sa_handler = sigchd_hand;
  sh.sa_flags = SA_RESTART;
  if (sigaction(SIGCHLD, &sh, NULL) == -1) {
    return -1;
  }
  alive = 1;
  return 0;
}

void sigdef_hand(int i)
{
  (void)i;
  alive = 0;
}

void sigchd_hand(int i)
{
  (void)i;
  int saved_errno = errno;
  while(waitpid(-1, NULL, WNOHANG) > 0);
  errno = saved_errno;
}

void sigalm_hand(int i)
{
  (void) i;
}

volatile int alive = 0;
