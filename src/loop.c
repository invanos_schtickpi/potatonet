#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <string.h>
#include <errno.h>
#include "loop.h"
#define STDIN_FD 0
#define STR_LNG 1024

static int complete_send(int socket, void const * buf, int len);
static int t_remote(struct ConnectionInfo * con);
static int f_remote(struct ConnectionInfo * con);
static ssize_t put_log(struct ConnectionInfo * con, void const * buff, size_t lng, int dir);
static ssize_t ip_log(struct ConnectionInfo * con);

int run_loop(struct ConnectionInfo * con)
{
  struct timespec tout;
  int (*compl)(struct ConnectionInfo *);
  fd_set readfds;
  fd_set backfd;
  int  recode;
  tout.tv_sec = 3;
  tout.tv_nsec = 500000000;
  FD_ZERO(&backfd);
  FD_SET(STDIN_FD, &backfd);
  FD_SET(con->socket, &backfd);
  ip_log(con);
  do{
    readfds = backfd;
    fputs("<<< ", stdout);
    fflush(stdout);
    recode = pselect(((con->socket > STDIN_FD) ? con->socket + 1: STDIN_FD + 1),
                     &readfds, NULL, NULL, &tout, NULL);
    if(recode < 0){
      perror("pselect error");
      return -1;
    }
    if(!recode){
      puts("No input. timing out.");
      return 0;
    }
    if(FD_ISSET(con->socket, &readfds)){
      compl = f_remote;
    }
    else{
      compl = t_remote;
    }
    if(compl(con)){
      return -1;
    }
  }while(1);
}

int t_remote(struct ConnectionInfo * con)
{
  char in[STR_LNG];
  size_t lng;
  if(fgets(in, STR_LNG, stdin) == NULL){
    return -1;
  }
  lng = strlen(in);
  if(complete_send(con->socket, in, lng) < 0){
    return -1;
  }
  if(put_log(con, in, lng, 0) < 0){
    return -1;
  }
  return 0;
}

int f_remote(struct ConnectionInfo * con)
{
  char in[STR_LNG];
  int recode = recv(con->socket, in, STR_LNG, 0);
  if(recode <= 0){
    return -1;
  }
  in[recode] = '\0';
  fputs("\b\b\b\b>>> ", stdout);
  puts(in);
  if(put_log(con, in, recode, 1) < 0){
    return -1;
  }
  return 0;
}

ssize_t put_log(struct ConnectionInfo * con, void const * buff, size_t lng, int dir)
{
  ssize_t sended;
  if(con->log.status == SCLOSED){
    return 0;
  }
  if(write(con->log.file, (dir ? ">>> " : "<<< "), strlen(">>> ")) < 0){
    return -1;
  }
  if((sended = write(con->log.file, buff, lng)) < 0){
    return -1;
  }
  if(dir){
    if(write(con->log.file, "\n", strlen("\n")) < 0){
      return -1;
    }
  }
  return sended;
}

ssize_t ip_log(struct ConnectionInfo * con)
{
  char const * ipget ="New connection to: ";
  if(con->log.status == SCLOSED){
    return 0;
  }
  if(write(con->log.file, ipget, strlen(ipget)) < 0){
    return -1;
  }
  if(write(con->log.file, con->ip, strlen(con->ip)) < 0){
    return -1;
  }
  if(write(con->log.file, "\n", strlen("\n")) < 0){
    return -1;
  }
  return 0;
}

int complete_send(int socket, void const * buf, int len)
{
  /*###########################################################*/
  /*BEEJ's function to send everything instead of partial send */
  /*all credits are to beej                                    */
  /*This function is modified by X33                           */
  /*to fit better with the usage in this program               */
  /*###########################################################*/
  int total = 0;
  int n;
  assert(buf);

  while(total < len) {
    n = send(socket, buf+total, (len - total), 0);
    if (n == -1) { break; }
    total += n;
  }
  return ((n == -1) ? -1 : total); // return -1 on failure, len on success
}
