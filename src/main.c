#include <stdio.h>
#include <errno.h>
#include "argparser.h"
#include "connect.h"
#include "signaling.h"
#include "loop.h"
#include "logging.h"
// /q = quit

int main(int argc, char ** argv)
{
  struct ConnectionError conStat;
  struct ConnectionInfo con = CONNECTION_INFO_INIT;
  parse_head(argc, argv, &con);
  if(con.domain == NULL){
    fputs("No domain supplied. Terminating program\n", stderr);
    return -1;
  }
  if(open_log(&con)){
    perror("Logging error: ");
    return -1;
  }
  conStat = init_connection(&con);
  if(conStat.status != NORMAL){
    perror("Connection: ");
    close_log(&con);
    return -1;
  }
  run_loop(&con);
  term_connection(&con);
  close_log(&con);
  return 0;
}
