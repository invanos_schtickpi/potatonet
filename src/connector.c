#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <netdb.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <errno.h>
#include "connect.h"

static int gen_addr_inf(struct ConnectionInfo * con,  struct addrinfo ** addrLink);
static struct ConnectionError assign_connection(struct ConnectionInfo * con,
                                                struct addrinfo * addrLink);

struct ConnectionError init_connection(struct ConnectionInfo * con)
{
  struct ConnectionError retCode;
  struct addrinfo * addrLink = NULL;
  //  int retCode;
  assert(con);
  if((retCode.err = gen_addr_inf(con, &addrLink)) != 0){
    con->socket = 0;
    con->status = CLOSED;
    retCode.status = IP_LOOKUP;
    return retCode;
  }
  retCode = assign_connection(con, addrLink);
  freeaddrinfo(addrLink);
  return retCode;
}

struct ConnectionError assign_connection(struct ConnectionInfo * con,  struct addrinfo * addrLink)
{
  struct ConnectionError retCode;
  for(; addrLink != NULL; addrLink = addrLink->ai_next){
    if((con->socket = socket(addrLink->ai_family, addrLink->ai_socktype, addrLink->ai_protocol))
       == -1){
      retCode.err = errno;
      retCode.status = SOCKET;
      continue;
    }
    if (connect(con->socket, addrLink->ai_addr, addrLink->ai_addrlen) == -1){
      close(con->socket);
      retCode.err = errno;
      retCode.status = CONNECT;
      continue;
    }
    break;
  }
  memset(con->ip, 0, IP_LEN);
  if (addrLink == NULL){
    con->socket = 0;
    con->status = CLOSED;
    return retCode;
  }
  retCode.err = errno;
  retCode.status = CONNECT;
  con->status = OPEN;
  if(addrLink->ai_family == AF_INET){
    //IP4
    struct sockaddr_in * ip = (struct sockaddr_in *)addrLink->ai_addr;
    inet_ntop(addrLink->ai_family, &(ip->sin_addr), con->ip, IP_LEN);
  }
  else{
    //IP6
    struct sockaddr_in6 * ip = (struct sockaddr_in6 *)addrLink->ai_addr;
    inet_ntop(addrLink->ai_family, &(ip->sin6_addr), con->ip, IP_LEN);
  }
  return (struct ConnectionError)CONNECTION_ERROR_SUCCESS;
}

int gen_addr_inf(struct ConnectionInfo * con, struct addrinfo ** addrLink)
{
  struct addrinfo hint;
  memset(&hint, 0, sizeof(struct addrinfo));
  hint.ai_family = AF_UNSPEC;
  hint.ai_socktype = SOCK_STREAM;
  hint.ai_flags = AI_PASSIVE;
  return getaddrinfo(con->domain, con->port, &hint, addrLink);
}

void term_connection(struct ConnectionInfo * con)
{
  assert(con);
  close(con->socket);
  con->status = CLOSED;
}
